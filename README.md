# 2019-02-25 08:51 - What would a plugin manager look like?

Until we have better names:

* PM: The plugin manager.
* PI: An instance of a plugin.
* ES: Event subscription - a plugin's subscription to some event.
* ER: Event (subscription) registry - a directory of registered events which are collected and sorted.
* EC: Event context.

What does it look like?

* PM has a concept of "installed", ie PI-1 and PI-2 are installed to the available codebase. [^1]
* PM has a concept of "enabled", ie PI-1 is installed and enabled; PI-2 is installed but disabled. Code from PI-2 is not loaded into memory; the application might provide a UI that lists this module as available.
* PM can source "available" PI via some external registry (gulp, Atom do this).
* PM can store state about which PI are enabled/installed/available, and may not always look this data up via expensive methods.
* PM can store state about which events are subscribed to by the enabled PI.
* PI event subscribers can return values (or promises).
* PM has an application base name when instantiated. [^2]
* PM has an application's version when instantiated. [^3]
* PI can declare the events they subscribe to. [^4]
* PI may trigger events themselves. [^5]
* PM must be robust and encapsulated.
* ES should have concepts of ordering, sorting. [^6]
* ES should have an interface to declare context (type of input). [^7]

Ideas:

* PM can set time limits on ES and bail out?

[^1] PIs might be installed to the regular node_modules directory, or might be installed outside the "core" app codebase. This would allow an application instance customisation without forking of the upstream codebase, because code changes were restricted to (say) `./plugins` folder.

[^2] Name here allows for the possibility of multiple plugin systems at once to use the same plugin engine. Namespace of event gets a prefix so `foo.user.delete` does not hit `bar.user.delete`.

[^3] This allows a plugin to define support for specific versions of an application. See eg Atom plugins "engines" entry in `package.json`

[^4] This result may be cached by PM, and PI wishing to dynamically ignore events should declare all events initially, then bail out early in the ES code path.

[^5] What does it look like when a PI triggers an event of its own? It can trigger them either for internal consumption (using its own plugin name as the application name) or for public consumption (using the current E application name as app name).

[^6] Events should be able to happen in order, but this suggests that later events must wait until earlier events are completed. Some plugins may be happy with asynchronous usage, so PI should be able to declare this per ES.

[^7] ES may wish to fail or ignore E which do not have expected context format. (Eg if type of input is not a User object, you could throw exception or skip, perhaps both app and plugin should have input to this?)
